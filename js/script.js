/**
 * This file defines the main logic of the webapp for MP1.
 */

// Define frequently used jQuery variables.
var $doc = $(document);
var $header = $('header');
var $sections = $('section');
var $indicators = $header.find('.section-list li');
var $modalOpenBtn = $('.open-modal-button');
var $modalCloseBtn = $('.close-modal-button');
var $modalSheet = $('.modal-sheet');

// Define the app.
var app = {
	initialize: function (){
		// Setup carousel.
		this.carousel = new Carousel('.carousel-container');

		// Execute the scroll handler in case position was saved.
		this.onScroll();

		// Hide modal.
		$modalSheet.hide();

		// Register event listeners.
		$doc.scroll(this.onScroll.bind(this));
		$indicators.click(this.onPositionIndicatorClick);
		$modalOpenBtn.click(this.openModal);
		$modalCloseBtn.click(this.closeModal);
	},

	openModal: function (){
		$modalSheet.fadeIn();
	},

	closeModal: function (){
		$modalSheet.fadeOut();
	},

	onScroll: function (e){
		var scrollTop = window.scrollY;
		this.updatePositionIndicators(scrollTop);

		// Update the header's size.
		if (scrollTop > 1){
			$header.addClass('mini');
		} else {
			$header.removeClass('mini');
		}
	},

	onPositionIndicatorClick: function (){
		var index = $indicators.index(this);
		var sectionTop = $($sections[index]).offset().top;
		var newScrollTop = sectionTop - $header.outerHeight();

		// Account for the shrunk navbar.
		if (!$header.hasClass('mini')){
			newScrollTop += 30;
		}
		$('html, body').stop().animate({'scrollTop': newScrollTop});
	},

	/**
	 * Updates the nav bar's position indicators.
	 * @param  {Number} scrollY The windows vertical scroll.
	 */
	updatePositionIndicators: function (scrollY){
		// Factor in the header's height when computing the current section.
		scrollY += $header.outerHeight();

		// Find the first section whose top offset comes after the window's 
		// current vertical scroll position.  Thus, we use the last section
		// as the default.
		var numSections = $sections.length;
		var belowIndex = numSections;
		var currSectionTop;
		for (var i=0; i < numSections; i++){
			currSectionTop = $($sections[i]).offset().top;

			if (currSectionTop > scrollY){
				// We want the first section below the window, so we break 
				// immediately.
				belowIndex = i;
				break;
			}
		}

		// With the current section index, update the corresponding position 
		// indicator in the nav bar.
		var currIndex = belowIndex < 1 ? 0 : belowIndex - 1;
		$indicators.removeClass('selected');
		if ($indicators[currIndex]){
			$($indicators[currIndex]).addClass('selected');
		}
	}
};

// Start the app when jQuery is ready.
$doc.ready(function (){
	app.initialize();
});
