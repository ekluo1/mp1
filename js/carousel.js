/**
 * This file contains logic related to the image carousel.
 */

/**
 * @param {string} selector The carousel container's selector.
 */
function Carousel (selector){
  var $el = this.$el = $(selector);
  this.imageWidth = $el.outerWidth();
  this.$leftArrow = $el.find('.left-arrow');
  this.$rightArrow = $el.find('.right-arrow');
  this.$images = $el.find('img')
      .css({'width': $el.outerWidth()});
  this.$carouselInner = $el.find('.carousel-inner')
      .css({'width': (this.$images.length*100)+'%'});
  this.$indicators = $el.find('.carousel-indicator');

  // Select the first indicator.
  this.$indicators.eq(0).addClass('selected');

  // State variables.
  this.currIndex = 0;

  // Register event handlers.
  this.$leftArrow.click(this.onLeftArrowClick.bind(this));
  this.$rightArrow.click(this.onRightArrowClick.bind(this));
  this.$indicators.click(this.onIndicatorClick.bind(this));
}

/**
 * @param {Number} newIndex The newly selected image index.
 */
Carousel.prototype.setIndex = function(newIndex){
  this.currIndex = newIndex;

  // Highlight the appropriate indicator.
  this.$indicators
      .removeClass('selected')
      .eq(newIndex)
      .addClass('selected');

  // Translate the inner carousel.
  var newTranslateX = -1*newIndex*this.imageWidth;
  this.$carouselInner.css({
    'transform': 'translate('+newTranslateX+'px, 0px)'
  });
};

Carousel.prototype.onIndicatorClick = function(evt){
  var newIndex = this.$indicators.index(evt.target);
  this.setIndex(newIndex);
};

Carousel.prototype.onLeftArrowClick = function(){
  // Calculate and set the new image index.
  var numImages = this.$images.length;
  var newIndex = (this.currIndex - 1 + numImages) % numImages;
  this.setIndex(newIndex);
};

Carousel.prototype.onRightArrowClick = function(){
  // Calculate and set the new image index.
  var newIndex = (this.currIndex + 1) % this.$images.length;
  this.setIndex(newIndex);
};
