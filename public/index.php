<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>MP1</title>
  <link rel="stylesheet" href="css/styles.css">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body>
  <header>
    <div class="title">
      <h1>reflections</h1>
      <ul class="section-list">
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
      </ul>
    </div>
  </header>
  <div id="main-container">
    <section data-section="intro">
      <div class="container">
        <i class="fa fa-quote-left"></i><span class="small-text"> Have you ever wondered why the sky </span><br><span>was so blue</span>?
      </div>
    </section>
    <section data-section="carousel">
      <div class="carousel-container">
        <div class="carousel">
          <div class="left-arrow arrow">&#10092;</div>
          <div class="right-arrow arrow">&#10093;</div>
          <div class="carousel-inner">
            <img src="img/pic1.jpg">
            <img src="img/pic2.jpg">
            <img src="img/pic3.jpg">
          </div>
        </div>
        <div class="lower-bar">
          <ul>
            <li class="carousel-indicator"></li>
            <li class="carousel-indicator"></li>
            <li class="carousel-indicator"></li>
          </ul>
        </div>
      </div>
    </section>
    <section data-section="multicol">
      <div class="container">
        <i class="fa fa-quote-left"></i><span class="small-text"> Or wondered why the grass </span><br><span>was always so green</span>?
      </div>
      <br>
      <div class="columns">
        <div class="col">
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
        </div>
        <div class="col">
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
        </div>
        <div class="col">
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
        </div>
      </div>
    </section>
    <section data-section="icons">
      <div class="vertical-container container">
        <p>What are the gears that turn...</p>
        <i class="fa fa-cog fa-spin fa-3x"></i>
        <i class="fa fa-cog fa-spin fa-5x"></i>
        <i class="fa fa-cog fa-spin fa-3x"></i>
      </div>
    </section>
    <section data-section="background vertical" class="fixed-background">
      <div class="vertical-container">
        <p class="quote">The Hands of Time?</p>
      </div>
    </section>
    <section data-section="modal video">
      <div class="container">
        <i class="fa fa-quote-left"></i><span class="small-text"> If you seek </span><br><span>the truth,</span>
      </div>
      <br>
      <ul class="centered-list">
        <li><button class="open-modal-button">click past</button></li>
        <li><button class="open-modal-button">the facade</button></li>
      </ul>
      <div class="modal-sheet">
        <div class="modal-video">
          <video src="http://v2v.cc/~j/theora_testsuite/320x240.ogg" autoplay loop></video>
        </div>
        <div class="close-modal-button">&times;</div>
      </div>
    </section>
    <section data-section="css-icons social media">
      <ul class="centered-list social-icons">
        <li><i class="fa fa-facebook-official fa-5x"></i></li>
        <li><i class="fa fa-pinterest-p fa-5x"></i></li>
        <li><i class="fa fa-whatsapp fa-5x"></i></li>
        <li><i class="fa fa-twitter-square fa-5x"></i></li>
        <li><i class="fa fa-tumblr fa-5x"></i></li>
        <li><i class="fa fa-google-plus fa-5x"></i></li>
      </ul>
      <br><br><br>
      <div class="container">
        <span class="small-text"> and share what you find :) </span>
      </div>
    </section>
  </div>
  <footer>
    <div class="title">
      F I N
    </div>
  </footer>

  <script src="lib/jquery-2.1.3.min/index.js"></script>
  <script src="js/script.js"></script>
  <script src="http://localhost:35729/livereload.js"></script>
</body>
</html>